package com.indra.selecaojava.service.mapper;

import java.util.List;
import java.util.stream.Collectors;
import org.springframework.stereotype.Component;
import com.indra.selecaojava.entity.PriceEntity;
import com.indra.selecaojava.model.GasStation;
import com.indra.selecaojava.model.Price;

@Component
public class PriceMapper {

  public List<Price> toModel(List<PriceEntity> entities) {
    return entities.stream().map(this::toModel).collect(Collectors.toList());
  }
  
  public Price toModel(PriceEntity entity) {
    Price price = new Price();
    price.setBrand(entity.getBrand());
    price.setGatheringDate(entity.getGatheringDate());
    price.setId(entity.getId());
    price.setMeasuringUnit(entity.getMeasuringUnit());
    price.setProduct(entity.getProduct());
    price.setPurchaseValue(entity.getPurchaseValue());
    price.setSaleValue(entity.getSaleValue());
    GasStation gasStation = new GasStation();
    if(entity.getGasStation() != null) {
      gasStation.setCity(entity.getGasStation().getCity());
      gasStation.setCounty(entity.getGasStation().getCounty());
      gasStation.setId(entity.getGasStation().getId());
      gasStation.setRegion(entity.getGasStation().getRegion());
      gasStation.setResale(entity.getGasStation().getResale());
    }
    price.setGasStation(gasStation);
    return price;
  }

}
