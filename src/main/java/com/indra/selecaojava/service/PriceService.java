package com.indra.selecaojava.service;

import java.io.IOException;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import org.apache.commons.lang3.StringUtils;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import com.indra.selecaojava.entity.GasStationEntity;
import com.indra.selecaojava.entity.PriceEntity;
import com.indra.selecaojava.model.GasStation;
import com.indra.selecaojava.model.Price;
import com.indra.selecaojava.repository.GasStationRepository;
import com.indra.selecaojava.repository.PriceRepository;
import com.indra.selecaojava.service.mapper.PriceMapper;

@Service
public class PriceService {


  private static final Logger log = LoggerFactory.getLogger(PriceService.class);


  @Autowired
  private ModelMapper modelMapper;

  @Autowired
  private PriceMapper priceMapper;

  @Autowired
  private PriceRepository priceRepository;

  @Autowired
  private GasStationRepository gasStationRepository;

  @Async
  public void saveFromFile(MultipartFile file) throws IOException {
    List<Price> prices = new ArrayList<>();
    Scanner scanner = new Scanner(file.getInputStream(), "Cp1252");
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    // skip header
    scanner.nextLine();
    while (scanner.hasNextLine()) {
      parseLine(prices, scanner, formatter);
    }
    scanner.close();

    this.save(prices);
  }

  private void parseLine(List<Price> prices, Scanner scanner, DateTimeFormatter formatter) {
    Scanner line;
    int i = 0;
    line = new Scanner(scanner.nextLine());
    line.useDelimiter("  ");
    Price price = new Price();
    GasStation gasStation = new GasStation();
    while (line.hasNext()) {
      String data = line.next();
      switch (i) {
        case 0:
          gasStation.setRegion(data);
          break;
        case 1:
          gasStation.setCounty(data);
          break;
        case 2:
          gasStation.setCity(data);
          break;
        case 3:
          gasStation.setResale(data);
          break;
        case 4:
          if (StringUtils.isNumeric(data)) {
            gasStation.setId(Long.valueOf(data));
          } else {
            gasStation.setResale(gasStation.getResale() + " " + data);
            i--;
          }
          break;
        case 5:
          price.setProduct(data);
          break;
        case 6:
          LocalDate date = LocalDate.parse(data, formatter);
          price.setGatheringDate(date);
          break;
        case 7:
          if (data.isEmpty()) {
            price.setPurchaseValue(null);
          } else {
            price.setPurchaseValue(new BigDecimal(data.replace(",", ".")));
          }
          break;
        case 8:
          price.setSaleValue(new BigDecimal(data.replace(",", ".")));
          break;
        case 9:
          price.setMeasuringUnit(data);
          break;
        case 10:
          price.setBrand(data);
          break;
        default:
          break;
      }
      i++;
    }
    price.setGasStation(gasStation);
    prices.add(price);
    line.close();
  }

  private void save(List<Price> prices) {
    int count = 0;
    for (Price price : prices) {
      GasStationEntity gasEntity = modelMapper.map(price.getGasStation(), GasStationEntity.class);
      gasStationRepository.save(gasEntity);
      PriceEntity priceEntity = modelMapper.map(price, PriceEntity.class);
      priceEntity.setGasStation(gasEntity);
      priceRepository.save(priceEntity);
      count++;
      if (count % 10000 == 0) {
        log.info("{} lines saved.", count);
      }
    }
    log.info("CSV upload finished. Total of lines: {}", count);
  }

  public Price save(Price price) {
    PriceEntity priceEntity = modelMapper.map(price, PriceEntity.class);
    if (price.getGasStation() != null
        && gasStationRepository.findById(price.getGasStation().getId()).isPresent()) {
      priceEntity.setGasStation(new GasStationEntity(price.getGasStation().getId()));
    } else {
      throw new IllegalArgumentException("Price needs to have a valid gas station.");
    }
    priceRepository.save(priceEntity);
    return modelMapper.map(priceRepository.save(priceEntity), Price.class);
  }

  public void update(Price price, Long id) {
    PriceEntity entity = priceRepository.findById(id).orElse(null);
    if (entity != null) {
      entity.setBrand(price.getBrand());
      entity.setGatheringDate(price.getGatheringDate());
      entity.setMeasuringUnit(price.getMeasuringUnit());
      entity.setProduct(price.getProduct());
      entity.setPurchaseValue(price.getPurchaseValue());
      entity.setSaleValue(price.getSaleValue());
      priceRepository.save(entity);
    } else {
      throw new IllegalArgumentException("The price you are trying to update does not exist.");
    }
  }

  public void delete(Long id) {
    if (priceRepository.findById(id).isPresent()) {
      priceRepository.deleteById(id);
    } else {
      throw new IllegalArgumentException("The price you are trying to delete does not exist.");
    }
  }

  public List<Price> find(String region, LocalDate gatheringDate, String brand, String city,
      String averageByCity, String averageByBrand, Pageable pageable) {
    if(averageByCity != null && !averageByCity.isEmpty()) {
      PriceEntity entity = priceRepository.findAverageByCity(averageByCity);
      return entity == null ? new ArrayList<>() : Arrays.asList(priceMapper.toModel(entity));
    }
    if(averageByBrand != null && !averageByBrand.isEmpty()) {
      PriceEntity entity = priceRepository.findAverageByBrand(averageByBrand);
      return entity == null ? new ArrayList<>() : Arrays.asList(priceMapper.toModel(entity));
    }
    List<PriceEntity> entities =
        priceRepository.findByFilters(region, gatheringDate, brand, city, pageable).getContent();
    return priceMapper.toModel(entities);
  }
}
