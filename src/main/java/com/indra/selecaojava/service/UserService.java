package com.indra.selecaojava.service;

import java.util.List;
import javax.validation.Valid;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import com.indra.selecaojava.entity.UserEntity;
import com.indra.selecaojava.model.User;
import com.indra.selecaojava.repository.UserRepository;
import com.indra.selecaojava.util.UserUtil;

@Service
public class UserService implements UserDetailsService {

  @Autowired
  private UserRepository userRepository;

  @Autowired
  private ModelMapper modelMapper;

  @Override
  public UserDetails loadUserByUsername(String username) {
    UserEntity userEntity = userRepository.findById(username).orElse(null);
    if (userEntity == null) {
      throw new UsernameNotFoundException("Invalid username or password.");
    }
    return modelMapper.map(userEntity, User.class);
  }

  public User save(User user) {
    if (userRepository.findById(user.getUsername()).orElse(null) == null) {
      UserEntity entity = modelMapper.map(user, UserEntity.class);
      return modelMapper.map(userRepository.save(entity), User.class);
    }
    throw new IllegalArgumentException("User already exists: " + user.getUsername());
  }

  public List<User> find() {
    return modelMapper.map(userRepository.findAll(), new TypeToken<List<User>>() {}.getType());
  }

  public void delete(String id) {
    if (UserUtil.getCurrentUser().getUsername().equals(id)) {
      userRepository.deleteById(id);
    } else {
      throw new IllegalArgumentException("Can not delete user other than yourself!");
    }
  }

  public void update(String id, @Valid User user) {
    if (UserUtil.getCurrentUser().getUsername().equals(id)) {
      if (!user.getUsername().equals(id)) {
        throw new IllegalArgumentException("Can not update username!");
      }
      UserEntity entity = modelMapper.map(user, UserEntity.class);
      userRepository.save(entity);
    } else {
      throw new IllegalArgumentException("Can not update user other than yourself!");
    }
  }

}
