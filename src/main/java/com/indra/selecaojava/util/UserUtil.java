package com.indra.selecaojava.util;

import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import com.indra.selecaojava.model.User;

@Component
public class UserUtil {

  private UserUtil() {}

  public static User getCurrentUser() {
    return (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

}
