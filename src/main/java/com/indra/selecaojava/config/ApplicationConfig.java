package com.indra.selecaojava.config;

import org.modelmapper.ModelMapper;
import org.springframework.boot.ApplicationRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import com.indra.selecaojava.entity.UserEntity;
import com.indra.selecaojava.repository.UserRepository;

@Configuration
@EnableAsync
public class ApplicationConfig {

  @Bean
  public ApplicationRunner initializer(UserRepository resourceRepository) {
    return args -> resourceRepository.save(
        new UserEntity("usuteste", "Usuário", "Teste", new BCryptPasswordEncoder().encode("1234")));
  }

  @Bean
  public ModelMapper modelMapper() {
    return new ModelMapper();
  }
}
