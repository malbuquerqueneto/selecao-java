package com.indra.selecaojava.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import com.indra.selecaojava.entity.GasStationEntity;

@Repository
public interface GasStationRepository extends JpaRepository<GasStationEntity, Long> {

}
