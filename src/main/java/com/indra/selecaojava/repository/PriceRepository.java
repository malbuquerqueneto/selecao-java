package com.indra.selecaojava.repository;

import java.time.LocalDate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import com.indra.selecaojava.entity.PriceEntity;

@Repository
public interface PriceRepository extends JpaRepository<PriceEntity, Long> {

  @Query("select p from PriceEntity p inner join p.gasStation as g where (g.region = :region or :region is null)"
      + "and (p.gatheringDate = :gatheringDate or :gatheringDate is null)"
      + "and (p.brand = :brand or :brand is null) and (g.city = :city or :city is null)")
  Page<PriceEntity> findByFilters(String region, LocalDate gatheringDate, String brand, String city,
      Pageable pageable);
  
  @Query("select new PriceEntity(avg(p.saleValue), avg(p.purchaseValue)) "
      + "from PriceEntity p inner join p.gasStation as g where g.city = :city group by g.city")
  PriceEntity findAverageByCity(String city);
  
  @Query("select new PriceEntity(avg(p.saleValue), avg(p.purchaseValue)) "
      + "from PriceEntity p where p.brand = :brand group by p.brand")
  PriceEntity findAverageByBrand(String brand);
}
