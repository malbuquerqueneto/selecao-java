package com.indra.selecaojava.entity;

import java.math.BigDecimal;
import java.time.LocalDate;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "prices")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PriceEntity {

  @Id
  @Column(unique = true)
  @GeneratedValue(strategy = GenerationType.AUTO)
  private Long id;

  private String product;

  private LocalDate gatheringDate;

  private BigDecimal saleValue;

  private BigDecimal purchaseValue;

  private String measuringUnit;

  private String brand;
  
  @ManyToOne
  @JoinColumn(name="gas_station_id", nullable=false)
  private GasStationEntity gasStation;
  
  public PriceEntity(double saleValue, double purchaseValue) {
    this.saleValue = BigDecimal.valueOf(saleValue);
    this.purchaseValue = BigDecimal.valueOf(purchaseValue);
  }
}
