package com.indra.selecaojava.entity;

import java.util.List;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "gas_stations")
@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class GasStationEntity {

  @Id
  @Column(unique = true)
  private Long id;
  
  private String region;
  
  private String county;
  
  private String city;
  
  private String resale;

  @OneToMany(fetch = FetchType.LAZY, mappedBy = "gasStation")
  private List<PriceEntity> prices;
  
  public GasStationEntity(Long id) {
    this.id = id;
  }
  
}
