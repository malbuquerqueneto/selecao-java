package com.indra.selecaojava.controller;

import java.time.LocalDate;
import java.util.List;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.indra.selecaojava.model.Price;
import com.indra.selecaojava.service.PriceService;

@RestController
@RequestMapping("/prices")
public class PriceController {

  @Autowired
  private PriceService priceService;

  @PostMapping(value = "/upload", consumes = MediaType.MULTIPART_FORM_DATA_VALUE)
  public ResponseEntity<Object> postUser(@RequestParam("file") MultipartFile file) {
    try {
      priceService.saveFromFile(file);
      return new ResponseEntity<>(HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @PostMapping(value = "")
  public ResponseEntity<Object> postPrice(@Valid @RequestBody Price price) {
    try {
      price = priceService.save(price);
      return new ResponseEntity<>(price, HttpStatus.CREATED);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }


  @GetMapping(value = "")
  public ResponseEntity<Object> getPrices(Pageable pageable,
      @RequestParam(name = "region", required = false) String region,
      @RequestParam(name = "gatheringDate",
          required = false) @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate gatheringDate,
      @RequestParam(name = "brand", required = false) String brand,
      @RequestParam(name = "averageByCity", required = false) String averageByCity,
      @RequestParam(name = "averageByBrand", required = false) String averageByBrand,
      @RequestParam(name = "city", required = false) String city) {
    try {
      List<Price> prices = priceService.find(region, gatheringDate, brand, city, averageByCity,
          averageByBrand, pageable);
      return new ResponseEntity<>(prices, prices.isEmpty() ? HttpStatus.NO_CONTENT : HttpStatus.OK);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @DeleteMapping(value = "/{id}")
  public ResponseEntity<Object> deletePrice(@PathVariable("id") Long id) {
    try {
      priceService.delete(id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

  @PutMapping(value = "/{id}")
  public ResponseEntity<Object> putPrice(@PathVariable("id") Long id,
      @Valid @RequestBody Price price) {
    try {
      priceService.update(price, id);
      return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    } catch (Exception e) {
      return new ResponseEntity<>(e.getMessage(), HttpStatus.BAD_REQUEST);
    }
  }

}
