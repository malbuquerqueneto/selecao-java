package com.indra.selecaojava.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDate;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Price implements Serializable {

  private static final long serialVersionUID = 7604958265793126344L;

  private Long id;

  private String product;

  private LocalDate gatheringDate;

  private BigDecimal purchaseValue;

  private BigDecimal saleValue;

  private String measuringUnit;

  private String brand;

  private GasStation gasStation;

}
