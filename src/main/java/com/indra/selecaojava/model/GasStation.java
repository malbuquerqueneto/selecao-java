package com.indra.selecaojava.model;

import java.io.Serializable;
import java.util.List;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GasStation implements Serializable {

  private static final long serialVersionUID = -5630586741555600750L;

  private String region;

  private String county;

  private String city;

  private String resale;

  private Long id;

  private List<Price> prices;

}
